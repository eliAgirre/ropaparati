MySQL

MySQL 5.5 database added.  Please make note of these credentials:

       Root User: adminVXv1Cm2
   Root Password: yVtMy8Msvq9R
   Database Name: shop
	    Host: 127.12.126.2
	    Port: 3306

Connection URL: mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/

You can manage your new MySQL database by also embedding phpmyadmin.
The phpmyadmin username and password will be the same as the MySQL credentials above.

--------------------------------------------------------------------------------------------

PHP My Admin

Please make note of these MySQL credentials again:
  Root User: adminVXv1Cm2
  Root Password: yVtMy8Msvq9R
URL: https://shop-proyectoapp.rhcloud.com/phpmyadmin/

--------------------------------------------------------------------------------------------

ClearDB

       Root User: bf6ef5fee2c909
   Root Password: 6afb6c23
   Database Name: ad_c336b3ebafe392d
	    Host: us-cdbr-iron-east-03.cleardb.net
	    Puerto: 3306
