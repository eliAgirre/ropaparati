CREATE DATABASE  IF NOT EXISTS `ad_15583adf16d2d86` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ad_15583adf16d2d86`;
-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (i686)
--
-- Host: us-cdbr-iron-east-02.cleardb.net    Database: ad_15583adf16d2d86
-- ------------------------------------------------------
-- Server version	5.5.45-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2016-02-03 16:23:55','2016-02-03 16:23:55','<p>La web \"ropaparati\" ha sido fundada para comprar ropa personalizada, tanto por ellos mismos como por otros usuarios.</p>\r\n\r\n<p>Puedes subir un patr&oacute;n y con ello podr&aacute;s ver c&oacute;mo encaja el patr&oacute;n en la ropa, como se puede visualizar en la siguiente imagen:</p><br />\r\n\r\n<center><img class=\"size-medium wp-image-8 aligncenter\" src=\"http://blog-proyectoapp.rhcloud.com/wp-content/uploads/2016/02/vestido_largo-199x300.jpg\" alt=\"vestido_largo\" width=\"199\" height=\"300\" /></center><br /><br />\r\n\r\n<p>Una vez que te guste, puedes publicar o no, t&uacute; decides. De esta forma podr&aacute;s comprar a t&uacute; gusto la prenda.</p>\r\n\r\n<p>Entra a nuestra web:&nbsp;<a href=\"http://ropaparati-proyectoapp.rhcloud.com/\">ropaparati</a></p>','Bienvenidos a ropaparati','','publish','open','open','','ropaparati','','','2016-02-03 17:03:26','2016-02-03 17:03:26','',0,'http://blog-proyectoapp.rhcloud.com/?p=1',0,'post','',0),(2,1,'2016-02-03 16:23:55','2016-02-03 16:23:55','Esto es una página de ejemplo. Es diferente a una entrada porque permanece fija en un lugar y se mostrará en la navegación de tu sitio (en la mayoría de los temas). La mayoría de la gente empieza con una página de Acerca de, que les presenta a los potenciales visitantes del sitio. Podría ser algo como esto:\n\n<blockquote>¡Hola! Soy mensajero por el día, aspirante a actor por la noche, y este es mi blog. Vivo en Madrid, tengo un perrazo llamado Duque y me gustan las piñas coladas (y que me pille un chaparrón)</blockquote>\n\n...o algo así:\n\n<blockquote>La empresa XYZ se fundó en 1971 y ha estado ofreciendo \"cosas\" de calidad al público desde entonces. Situada en Madrid, XYZ emplea a más de 2.000 personas y hace todo tipo de cosas sorprendentes para la comunidad de Madrid.</blockquote>\n\nSi eres nuevo en WordPress deberías ir a <a href=\"https://blog-proyectoapp.rhcloud.com/wp-admin/\">tu escritorio</a> para borrar esta página y crear páginas nuevas con tu propio contenido. ¡Pásalo bien!','Página de ejemplo','','trash','closed','open','','pagina-ejemplo','','','2016-02-03 17:15:11','2016-02-03 17:15:11','',0,'http://blog-proyectoapp.rhcloud.com/?page_id=2',0,'page','',0),(3,1,'2016-02-03 16:24:21','0000-00-00 00:00:00','','Borrador automático','','auto-draft','open','open','','','','','2016-02-03 16:24:21','0000-00-00 00:00:00','',0,'http://blog-proyectoapp.rhcloud.com/?p=3',0,'post','',0),(7,1,'2016-02-03 16:45:51','2016-02-03 16:45:51','La web \"ropaparati\" ha sido fundada para comprar ropa personalizada, tanto por ellos mismos como por otros usuarios.\n\nPuedes subir un patrón y con ello podrás ver cómo encaja el patrón en la ropa, como se puede visualizar en la siguiente imagen:\n\n<img class=\"size-medium wp-image-8 aligncenter\" src=\"http://blog-proyectoapp.rhcloud.com/wp-content/uploads/2016/02/vestido_largo-199x300.jpg\" alt=\"vestido_largo\" width=\"199\" height=\"300\" />\n\nUna vez que te guste, puedes publicar o no, tú decides. De esta forma podrás comprar a tú gusto la prenda.','Bienvenidos a ropaparati','','inherit','closed','closed','','1-autosave-v1','','','2016-02-03 16:45:51','2016-02-03 16:45:51','',1,'http://blog-proyectoapp.rhcloud.com/2016/02/03/1-autosave-v1/',0,'revision','',0),(8,1,'2016-02-03 16:43:14','2016-02-03 16:43:14','','vestido_largo','','inherit','open','closed','','vestido_largo','','','2016-02-03 16:43:14','2016-02-03 16:43:14','',1,'http://blog-proyectoapp.rhcloud.com/wp-content/uploads/2016/02/vestido_largo.jpg',0,'attachment','image/jpeg',0),(9,1,'2016-02-03 16:46:06','2016-02-03 16:46:06','La web \"ropaparati\" ha sido fundada para comprar ropa personalizada, tanto por ellos mismos como por otros usuarios.\r\n\r\nPuedes subir un patrón y con ello podrás ver cómo encaja el patrón en la ropa, como se puede visualizar en la siguiente imagen:\r\n\r\n<img class=\"size-medium wp-image-8 aligncenter\" src=\"http://blog-proyectoapp.rhcloud.com/wp-content/uploads/2016/02/vestido_largo-199x300.jpg\" alt=\"vestido_largo\" width=\"199\" height=\"300\" />\r\n\r\nUna vez que te guste, puedes publicar o no, tú decides. De esta forma podrás comprar a tú gusto la prenda.','Bienvenidos a ropaparati','','inherit','closed','closed','','1-revision-v1','','','2016-02-03 16:46:06','2016-02-03 16:46:06','',1,'http://blog-proyectoapp.rhcloud.com/2016/02/03/1-revision-v1/',0,'revision','',0),(10,1,'2016-02-03 17:03:26','2016-02-03 17:03:26','La web \"ropaparati\" ha sido fundada para comprar ropa personalizada, tanto por ellos mismos como por otros usuarios.\r\n\r\nPuedes subir un patrón y con ello podrás ver cómo encaja el patrón en la ropa, como se puede visualizar en la siguiente imagen:\r\n\r\n<img class=\"size-medium wp-image-8 aligncenter\" src=\"http://blog-proyectoapp.rhcloud.com/wp-content/uploads/2016/02/vestido_largo-199x300.jpg\" alt=\"vestido_largo\" width=\"199\" height=\"300\" />\r\n\r\nUna vez que te guste, puedes publicar o no, tú decides. De esta forma podrás comprar a tú gusto la prenda.\r\n\r\nEntra a nuestra web: <a href=\"http://ropaparati-proyectoapp.rhcloud.com/\">ropaparati</a>','Bienvenidos a ropaparati','','inherit','closed','closed','','1-revision-v1','','','2016-02-03 17:03:26','2016-02-03 17:03:26','',1,'http://blog-proyectoapp.rhcloud.com/2016/02/03/1-revision-v1/',0,'revision','',0),(11,1,'2016-02-03 17:15:11','2016-02-03 17:15:11','Esto es una página de ejemplo. Es diferente a una entrada porque permanece fija en un lugar y se mostrará en la navegación de tu sitio (en la mayoría de los temas). La mayoría de la gente empieza con una página de Acerca de, que les presenta a los potenciales visitantes del sitio. Podría ser algo como esto:\n\n<blockquote>¡Hola! Soy mensajero por el día, aspirante a actor por la noche, y este es mi blog. Vivo en Madrid, tengo un perrazo llamado Duque y me gustan las piñas coladas (y que me pille un chaparrón)</blockquote>\n\n...o algo así:\n\n<blockquote>La empresa XYZ se fundó en 1971 y ha estado ofreciendo \"cosas\" de calidad al público desde entonces. Situada en Madrid, XYZ emplea a más de 2.000 personas y hace todo tipo de cosas sorprendentes para la comunidad de Madrid.</blockquote>\n\nSi eres nuevo en WordPress deberías ir a <a href=\"https://blog-proyectoapp.rhcloud.com/wp-admin/\">tu escritorio</a> para borrar esta página y crear páginas nuevas con tu propio contenido. ¡Pásalo bien!','Página de ejemplo','','inherit','closed','closed','','2-revision-v1','','','2016-02-03 17:15:11','2016-02-03 17:15:11','',2,'http://blog-proyectoapp.rhcloud.com/2016/02/03/2-revision-v1/',0,'revision','',0),(12,1,'2016-02-03 17:51:08','2016-02-03 17:51:08','','logo_wordpress','','inherit','open','closed','','logo_wordpress','','','2016-02-03 17:52:46','2016-02-03 17:52:46','',0,'http://blog-proyectoapp.rhcloud.com/wp-content/uploads/2016/02/logo_wordpress.png',0,'attachment','image/png',0),(13,1,'2016-02-03 17:51:29','2016-02-03 17:51:29','http://blog-proyectoapp.rhcloud.com/wp-content/uploads/2016/02/cropped-logo_wordpress.png','cropped-logo_wordpress.png','','inherit','open','closed','','cropped-logo_wordpress-png','','','2016-02-03 17:51:29','2016-02-03 17:51:29','',0,'http://blog-proyectoapp.rhcloud.com/wp-content/uploads/2016/02/cropped-logo_wordpress.png',0,'attachment','image/png',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-11 18:08:00
